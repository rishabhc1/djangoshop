from django.conf import settings
from cms.sitemaps import CMSSitemap
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.http import HttpResponse
from django.urls import path, include, re_path
from rest_framework.routers import DefaultRouter
from shop.views.cart import CartViewSet
from shop.views.catalog import ProductRetrieveView, ProductListView, AddToCartView

from jewelleryshop.sitemap import ProductSitemap
from jewelleryshop.views import ProductDetails,  CheckoutViewSet1

router = DefaultRouter()
router.register("checkout1", CheckoutViewSet1, basename="checkout1")

sitemaps = {'cmspages': CMSSitemap,
            'products': ProductSitemap}


def render_robots(request):
    permission = 'noindex' in settings.ROBOTS_META_TAGS and 'Disallow' or 'Allow'
    return HttpResponse('User-Agent: *\n%s: /\n' % permission, content_type='text/plain')


i18n_urls = (
    # url(r'^admin/', admin.site.urls),
    url(r'^', include('cms.urls')),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^robots\.txt$', render_robots),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='sitemap'),
    path('shop/', include('shop.urls')),
    path('product/summary/', ProductDetails.as_view({'get': 'list'}), name='product'),
    path('add-to-cart1', CartViewSet.as_view({'post'}), name='add-to-cart1'),
    # path('checkout1/', CheckoutViewSet1, name='checkout1'),
    path("v1/", include(router.urls)),
    path("v1/", include('jewelleryshop.urls')),


]

if settings.USE_I18N:
    urlpatterns.extend(i18n_patterns(*i18n_urls))
else:
    urlpatterns.extend(i18n_urls)
urlpatterns.extend(
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT))
