# from django.urls import path
# from shop.serializers.bases import ProductSerializer
# from shop.views.catalog import ProductListView

# from jewelleryshop.views import ProductSummary
from django.urls import re_path, path
from shop.views.catalog import ProductRetrieveView

from jewelleryshop.views import CkProductListView

urlpatterns = [
    # path('summary/', ProductSummary.as_view(), name='product'),

    re_path(r'^products/(?P<slug>[\w-]+)/?$', ProductRetrieveView.as_view(lookup_field="id")),
    path('products', CkProductListView.as_view(), name='products'),
]
