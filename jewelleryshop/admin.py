from adminsortable2.admin import PolymorphicSortableAdminMixin, SortableAdminMixin
from cms.admin.placeholderadmin import PlaceholderAdminMixin, FrontendEditableAdminMixin
from django.contrib import admin

# Register your models here.
from parler.admin import TranslatableAdmin
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelFilter, PolymorphicChildModelAdmin
from shop.admin.defaults.customer import CustomerAdmin
from django.utils.translation import ugettext_lazy as _
from shop.admin.defaults.order import OrderAdmin
from shop.admin.defaults import customer
from shop.admin.delivery import DeliveryOrderAdminMixin
from shop.admin.order import PrintInvoiceAdminMixin
from shop.admin.product import InvalidateProductCacheMixin, SearchProductIndexMixin, CMSPageAsCategoryMixin, \
    ProductImageInline
from shop.models.defaults.cart import Cart
from shop.models.defaults.cart_item import CartItem
from shop.models.defaults.customer import Customer
from shop.models.defaults.order import Order

# from djangoshop.jewelleryshop.models import Product
# from tests.models import Product
from jewelleryshop.models import Product, JewelleryInventory, Jewellery, Manufacturer, Commodity, CommodityInventory


class SendCloudOrderAdminMixin(object):
    pass


admin.site.register(Manufacturer, admin.ModelAdmin)


@admin.register(Order)
class OrderAdmin(PrintInvoiceAdminMixin, SendCloudOrderAdminMixin, DeliveryOrderAdminMixin, OrderAdmin):
    pass


# admin.site.register(Customer, CustomerAdmin)

__all__ = ['customer']


class CommodityInventoryAdmin(admin.StackedInline):
    model = CommodityInventory
    extra = 0


class JewelleryInventoryAdmin(admin.StackedInline):
    model = JewelleryInventory
    extra = 0


@admin.register(Product)
class ProductAdmin(PolymorphicSortableAdminMixin, PolymorphicParentModelAdmin):
    base_model = Product
    child_models = [Jewellery]
    list_display = ['product_name', 'get_price', 'product_type', 'active']
    list_display_links = ['product_name']
    search_fields = ['product_name']
    list_filter = [PolymorphicChildModelFilter]

    # list_per_page = 250
    # list_max_show_all = 1000

    def get_price(self, obj):
        return str(obj.get_real_instance().get_price(None))

    get_price.short_description = _("Price starting at")


@admin.register(Commodity)
class CommodityAdmin(InvalidateProductCacheMixin, SearchProductIndexMixin, SortableAdminMixin, TranslatableAdmin,
                     FrontendEditableAdminMixin,
                     PlaceholderAdminMixin, PolymorphicChildModelAdmin):
    """
    Since our Commodity model inherits from polymorphic Product, we have to redefine its admin class.
    """
    base_model = Product
    fields = [
        ('product_name', 'slug'),
        ('product_code', 'unit_price'),
        'active',
        'caption',
        'manufacturer',
    ]
    # filter_horizontal = ['cms_pages']
    inlines = [ProductImageInline, CommodityInventoryAdmin]
    prepopulated_fields = {'slug': ['product_name']}


@admin.register(Jewellery)
class JewelleryAdmin(InvalidateProductCacheMixin, SearchProductIndexMixin, SortableAdminMixin, TranslatableAdmin,
                     PlaceholderAdminMixin, PolymorphicChildModelAdmin):
    base_model = Product
    fieldsets = (
        (None, {
            'fields': [
                ('product_name', 'slug'),
                ('product_code', 'unit_price'),
                'active',
            ],
        }),
        (_("Translatable Fields"), {
            'fields': ['caption', 'description'],
        }),
        (_("Properties"), {
            'fields': ['manufacturer'],
        }),
    )
    # filter_horizontal = ['cms_pages']
    inlines = [ProductImageInline, JewelleryInventoryAdmin]
    prepopulated_fields = {'slug': ['product_name']}


admin.site.register(Cart)

admin.site.register(CartItem)
