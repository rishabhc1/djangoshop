# Generated by Django 3.0.13 on 2021-03-02 12:03

from django.db import migrations, models
import django.db.models.deletion
import djangocms_text_ckeditor.fields
import parler.models


class Migration(migrations.Migration):

    dependencies = [
        ('jewelleryshop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('language_code', models.CharField(db_index=True, max_length=15, verbose_name='Language')),
                ('caption', djangocms_text_ckeditor.fields.HTMLField(blank=True, help_text="Short description used in the catalog's list view of products.", null=True, verbose_name='Caption')),
                ('master', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='jewelleryshop.Product')),
            ],
            options={
                'unique_together': {('language_code', 'master')},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
    ]
