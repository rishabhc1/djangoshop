import logging
import os

from django.apps import AppConfig
from django.conf import settings


class JewelleryshopConfig(AppConfig):
    name = 'jewelleryshop'
    verbose_name = _("My Shop")
    logger = logging.getLogger('myshop')

    def ready(self):
        if not os.path.isdir(settings.STATIC_ROOT):
            os.makedirs(settings.STATIC_ROOT)
        if not os.path.isdir(settings.MEDIA_ROOT):
            os.makedirs(settings.MEDIA_ROOT)
        if hasattr(settings, 'COMPRESS_ROOT') and not os.path.isdir(settings.COMPRESS_ROOT):
            os.makedirs(settings.COMPRESS_ROOT)
        as_i18n = " as I18N"
        self.logger.info("Running as polymorphic{}".format(as_i18n))

        from jewelleryshop import search_indexes
        __all__ = ['search_indexes']