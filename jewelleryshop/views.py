from django.db import transaction
from django.shortcuts import render

# Create your views here.
from rest_framework import generics, viewsets, status
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, ListModelMixin
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.response import Response
from shop.models.cart import CartModel
from shop.rest.money import JSONRenderer

from shop.serializers.defaults.product_summary import ProductSummarySerializer
from shop.views.catalog import ProductListView
from shop.views.checkout import CheckoutViewSet

from jewelleryshop.models import Jewellery, Product
from jewelleryshop.serializers import ProductListSerializer


class ProductDetails(CreateModelMixin, ListModelMixin, viewsets.GenericViewSet):
    serializer_class = ProductListSerializer

    def get_queryset(self):
        return Jewellery.objects.all()


class CkProductListView(ProductListView):
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)


class CheckoutViewSet1(CheckoutViewSet):

    # def __init__(self):
    #     # super().__init__(**kwargs)
    #     pass

    @action(methods=['put'], detail=False, url_path='upload')
    def upload(self, request):
        """
        Use this REST endpoint to upload the payload of all forms used to setup the checkout
        dialogs. This method takes care to dispatch the uploaded payload to each corresponding
        form.
        """
        # sort posted form data by plugin order
        # cart = CartModel.objects.get_from_request(request)
        # dialog_data = []
        # for form_class in self.dialog_forms:
        #     if form_class.scope_prefix in request.data:
        #         if 'plugin_order' in request.data[form_class.scope_prefix]:
        #             dialog_data.append((form_class, request.data[form_class.scope_prefix]))
        #         else:
        #             for data in request.data[form_class.scope_prefix].values():
        #                 dialog_data.append((form_class, data))
        # dialog_data = sorted(dialog_data, key=lambda tpl: int(tpl[1]['plugin_order']))
        #
        # # save data, get text representation and collect potential errors
        # errors, response_data, set_is_valid = {}, {}, True
        # with transaction.atomic():
        #     for form_class, data in dialog_data:
        #         form = form_class.form_factory(request, data, cart)
        #         if form.is_valid():
        #             # empty error dict forces revalidation by the client side validation
        #             errors[form_class.form_name] = {}
        #         else:
        #             errors[form_class.form_name] = form.errors
        #             set_is_valid = False
        #
        #         # by updating the response data, we can override the form's content
        #         update_data = form.get_response_data()
        #         if isinstance(update_data, dict):
        #             response_data[form_class.form_name] = update_data
        #
        #     # persist changes in cart
        #     if set_is_valid:
        #         cart.save()
        #
        # # add possible form errors for giving feedback to the customer
        # if set_is_valid:
        #     return Response(response_data)
        # else:
        #     return Response(errors, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        pass

    @action(methods=['get'], detail=False, url_path='digest')
    def digest(self, request):
        """
        Returns the summaries of the cart and various checkout forms to be rendered in non-editable fields.
        """
        cart = CartModel.objects.get_from_request(request)
        cart.update(request)
        # context = self.get_serializer_context()
        checkout_serializer = self.serializer_class(cart, label=self.serializer_label)
        cart_serializer = self.cart_serializer_class(cart, label='cart')
        response_data = {
            'checkout_digest': checkout_serializer.data,
            'cart_summary': cart_serializer.data,
        }
        return Response(data=response_data)