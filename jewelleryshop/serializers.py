from rest_framework import viewsets, serializers
from shop.serializers.bases import ProductSerializer
from rest_framework.mixins import CreateModelMixin, ListModelMixin

from jewelleryshop.models import Product, Jewellery
# from jewelleryshop.views import ProductDetails


# class ProductSummarySerializer(ProductDetails):
#     class Meta:
#         model = Jewellery
#         fields = ['id', 'product_name', 'product_url',
#                   'product_type', 'product_model', 'price']

# class ProductListSerializer(CreateModelMixin, ListModelMixin, viewsets.GenericViewSet):
#     serializer_class = Jewellery
#
#     def get_queryset(self):
#         return Jewellery.objects.filter(user=self.request.user)


class ProductListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Jewellery
        fields = "__all__"
